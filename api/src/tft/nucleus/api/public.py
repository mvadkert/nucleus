# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides public implementation of testing farm API.
"""
from typing import Dict

from fastapi import FastAPI
from fastapi_versioning import VersionedFastAPI, version

from .routers import test_request, compose
from .about import about_get


api = FastAPI(
    title='Testing Farm API',
)


@api.get('/about', summary='About Testing Farm')
@version(0, 1)  # type: ignore
def get_about() -> Dict[str, str]:
    """
    The function returns metadata about nucleus api package.
    """
    return about_get()


api.include_router(test_request.router)
api.include_router(compose.router)


# This line should be at the end of the file
api = VersionedFastAPI(
    api,
    version_format='{major}.{minor}',
    prefix_format='/v{major}.{minor}'
)
