# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of test requests router
"""
from fastapi import APIRouter
from fastapi_versioning import version

from ..schemes import test_request

router = APIRouter()


# TODO: it does nothing, for working docs
@router.post('/requests', response_model=test_request.RequestCreateOut)
@version(0, 1)  # type: ignore
def request_a_new_test(request: test_request.RequestCreateIn) -> test_request.RequestCreateOut:
    """
    Create new test request handler
    """
    return request  # type: ignore


# TODO: it does nothing, for working docs
@router.get('/requests', response_model=test_request.RequestGetOut)
@version(0, 1)  # type: ignore
def test_request_details(request: test_request.RequestGetIn) -> test_request.RequestGetOut:
    """
    Get test request handler
    """
    return request  # type: ignore
